#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import requests
import os
import sklearn import tree
import nltk
from nltk.tokenized import work_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

import itertools


# In[ ]:


#Notes files 1,2,3 are saved in the path
file1 = pd.read_csv(path+"/"+"File1.csv")
file2 = pd.read_csv(path+"/"+"File2.csv")
file3 = pd.read_csv(path+"/"+"File3.csv")


# In[ ]:


nltk.download('punkt')
nltk.download('stopwords')

sw = set(stopwords.words('english'))
ps = PoterStemmer()


# In[ ]:


n1 = len(file["NOTES"])
n2 = len(file["NOTES"])
n3 = len(file["NOTES"])


# In[ ]:


CSN_1 = []
for i in range(n1):
    CSN_1.append(file1["PAT_ENC_CSN_ID"].loc[i])
CSN_2 = []
for i in range(n2):
    CSN_2.append(file2["PAT_ENC_CSN_ID"].loc[i])
CSN_3 = []
for i in range(n3):
    CSN_3.append(file3["PAT_ENC_CSN_ID"].loc[i])


# In[ ]:


snetences_1 = []
for i in range(0,n1):
    sentences_1.append(file1["NOTES"].loc[i])
snetences_2 = []
for i in range(0,n2):
    sentences_2.append(file2["NOTES"].loc[i])
snetences_3 = []
for i in range(0,n3):
    sentences_3.append(file3["NOTES"].loc[i])


# In[ ]:


y1 = []
for i in range(0,n1):
    y1.append(file1["COVID_HOSP"].loc[i])
y2 = []
for i in range(0,n2):
    y2.append(file2["COVID_HOSP"].loc[i])
y3 = []
for i in range(0,n3):
    y3.append(file3["COVID_HOSP"].loc[i])


# In[ ]:


y_1 = []
for i in range(n1):
    if y1[i]=="N":
        y_1.append(0)
    elif y1[i]=="Y":
        y_1.append(1)
y_2 = []
for i in range(n2):
    if y2[i]=="N":
        y_2.append(0)
    elif y2[i]=="Y":
        y_2.append(1)
y_3 = []
for i in range(n3):
    if y3[i]=="N":
        y_3.append(0)
    elif y3[i]=="Y":
        y_3.append(1)


# In[ ]:


def tokenize(sentences):
    return[
        ps.stem(w.lower())
        for w in word_tokenize(sentence)
        if w.replace("''","",1).isalpha() and (w not in sw)
    ]


# In[ ]:


s1 = len(sentences_1)
s2 = len(sentences_2)
s3 = len(sentences_3)
sentences1 = []
for i in range(0,s1):
    sentences1.append(str(sentences_1[i]))
sentences2 = []
for i in range(0,s2):
    sentences2.append(str(sentences_2[i]))
sentences1 = []
for i in range(0,s3):
    sentences3.append(str(sentences_3[i]))


# In[ ]:


tokenized_sentences_1 = [tokenize(s) for s in sentences1]
tokenized_sentences_2 = [tokenize(s) for s in sentences2]
tokenized_sentences_3 = [tokenize(s) for s in sentences3]


# In[ ]:


vcs_1 = pd.value_counts([w for s in tokenized_sentences_1 for w in s])
vcs_2 = pd.value_counts([w for s in tokenized_sentences_2 for w in s])
vcs_3 = pd.value_counts([w for s in tokenized_sentences_3 for w in s])
vcs_1.head(10)


# In[ ]:


vocabulary_1 = vcs_1.index.values[vcs_1 >= 50]
len(vocabulary_1)
vocabulary_2 = vcs_2.index.values[vcs_2 >= 50]
len(vocabulary_2)
vocabulary_3 = vcs_3.index.values[vcs_3 >= 50]
len(vocabulary_3)


# In[ ]:


vocab_dict1 = {word:idx for idx, word in enumerate(vocabulary_1)}
vocab_dict2 = {word:idx for idx, word in enumerate(vocabulary_2)}
vocab_dict3 = {word:idx for idx, word in enumerate(vocabulary_3)}


# In[ ]:


keylist1 = []
for i in range(len(vocabulary_1)):
    keylist1.append(vocabulary_1[i])
keylist1.append('y')
keylist1.append('PAT_ENC_CSN_ID')

keylist2 = []
for i in range(len(vocabulary_2)):
    keylist2.append(vocabulary_2[i])
keylist2.append('y')
keylist2.append('PAT_ENC_CSN_ID')

keylist3 = []
for i in range(len(vocabulary_3)):
    keylist3.append(vocabulary_3[i])
keylist3.append('y')
keylist3.append('PAT_ENC_CSN_ID')


# In[ ]:


def word_counts(tokens_list,vocab_dict):
    vocab_words = vocab_dict.keys()
    counts = np.zeros(len(tokens_list),len(vocab_words))
    for idx, tokens in enumerate(token_list):
        for token in tokens:
            if token in vocab_words:
                counts[idx, vocab_dict[token]] += 1
    return counts
def term_freq(counts):
    row_total = np.sum(counts, axis=1) +1
    assert np.amin(row_totals) > 0
    return counts / row_total[:, np.newaxis]

def document_freq(counts):
    col_totals = np.sum((counts > 0).astype('int'), axis=0)
    return col_totals[np.newaxis,:] / (len(counts) + 1)


# In[ ]:


output_f1 = sentences1[:]
print(len(output_f1))


# In[1]:


#File 1
output_f1 = sentences1[:]

tokens_f1 = [tokenize(s) for s in output_f1]

counts_f1 = word_counts(tokens_f1, vocab_dict1).astype(np.float16)

df = document_freq(counts_f1)
x_f1 = term_freq(counts_f1) / df

df = pd.DataFrame(x_f1)
df['y'] = y_1[:]
df['PAT_ENC_CSN_ID'] = CSN_1[:]
np.savetxt('File1 matrix.csv', df, delimiter=",", fmt='% s',encoding='utf-8')


# In[ ]:


#File 2
output_f2 = sentences2[:]

tokens_f2 = [tokenize(s) for s in output_f2]

counts_f2 = word_counts(tokens_f2, vocab_dict2).astype(np.float16)

df = document_freq(counts_f2)
x_f2 = term_freq(counts_f2) / df

df = pd.DataFrame(x_f2)
df['y'] = y_2[:]
df['PAT_ENC_CSN_ID'] = CSN_2[:]
np.savetxt('File1 matrix.csv', df, delimiter=",", fmt='% s',encoding='utf-8')


# In[ ]:


#File 3
output_f3 = sentences1[:]

tokens_f3 = [tokenize(s) for s in output_f3]

counts_f3 = word_counts(tokens_f3, vocab_dict3).astype(np.float16)

df = document_freq(counts_f3)
x_f3 = term_freq(counts_f3) / df

df = pd.DataFrame(x_f3)
df['y'] = y_3[:]
df['PAT_ENC_CSN_ID'] = CSN_3[:]
np.savetxt('File1 matrix.csv', df, delimiter=",", fmt='% s',encoding='utf-8')

